<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

class Order
{

    public static $cart_lifetime = 20;
    
    private static function priceDelivery($payment_method, $price_products, $vaucher = 0, $promo = 0, $promo_discount = 0)
    {
    	if ($promo_discount < 0)
    	{
    		$promo_discount = $promo_discount * -1;
    	}
    	
    	// ако платежния метод е наложен платеж
    	if ($payment_method == 3)
    	{
    		if ($price_products - $vaucher - $promo - $promo_discount < 150)
    		{
    			return 5;
    		}
    	}
    	else
    	{
    		if ($price_products - $vaucher - $promo - $promo_discount < 45)
    		{
    			return 5;
    		}
    	}
    	return 0;
    }

    public static function get($order_id, $user_id = 0, $promo_discount = 0/*, $new_price_balance_promo = FALSE*/)
    {
        $sql = 'SELECT * FROM `orders` WHERE `order_id` = ' . escape($order_id);
        $result = mysql_query($sql);
        $order = mysql_fetch_assoc($result);
        
        $order['user_status_name'] = Order::getUserStatus($order['user_status']);
        $order['user_status_name'] = $order['user_status_name']['name_bg'];

        $order['payment_status_name'] = Order::getPaymentStatus($order['payment_status']);
        $order['payment_status_name'] = $order['payment_status_name']['name_bg'];

        $order['order_status_name'] = Order::getOrderStatus($order['order_status']);
        $order['order_status_name'] = $order['order_status_name']['name_bg'];

        // избиране на всички продукти, които са резервирани
        $sql = '
            SELECT *
            FROM `order_products`
            WHERE
                `order_id` = ' . escape($order_id) . '
                AND `reserved` > 0';
        $result = mysql_query($sql);

        // променливи за всички цени
        $price_products = 0;
        $price_balance_promo = 0;
        $price_balance = 0;
        $price_delivery = 0;
        $price_total = 0;
        $from_club = null;

        // обхождане на продуктите
        if (mysql_num_rows($result) > 0)
        {
            $order['products'] = array();

            $products_quantity = 0;

            while ($product = mysql_fetch_assoc($result))
            {
                $prices = Product::getVariantPrices($product['product_id'], $product['variant_internal'], 0);
                $order['products'][] = array_merge($product);
                if ($from_club === null)
                {
                    $from_club = Product::get($product['product_id'])['collection_id'] > 0;
                }

                $price_products += $prices['price_customer'] * $product['quantity'];
                $price_balance_promo += $prices['max_promo_balance'] * $product['quantity'];

                $products_quantity += $product['quantity'];
            }
        }
        

        $user = User::get($user_id);
        
        // ако потребителя не иска да ползва целия или част от промо ваучера за тази поръчка
        if (isset($_SESSION['new_price_balance_promo']) && $_SESSION['new_price_balance_promo'] !== FALSE)
        {
        	$user['balance_promo'] = $_SESSION['new_price_balance_promo'];
        }
        
        // user_id се слага когато се изчислява order-а / ако няма user_id, не изчислява персоналните цени
        if ($user['user_id'] > 0)
        {
        	// ако има реален промо баланс се взима само той под внимание и не се гледа ваучера
            if ($user['balance_real'] > 0
                && ($user['balance_real_valid_for'] === null
                    || ($user['balance_real_valid_for'] == 1 && $from_club)
                    || ($user['balance_real_valid_for'] == 2 && !$from_club)))
        	{
        		$price_balance_promo = 0;
        		$de = Order::priceDelivery($order['payment_method_id'], $price_products, 0, $user['balance_real']);
        		if ($price_products + $de >= $user['balance_real'])
        		{
                    $price_balance = $user['balance_real'];
        		}
        		else 
        		{
                    $price_balance = $price_products + $de;
        		}
                
        	}
            else if ($price_balance_promo >= $user['balance_promo']
                && ($user['balance_promo_valid_for'] === null
                    || ($user['balance_promo_valid_for'] == 1 && $from_club)
                    || ($user['balance_promo_valid_for'] == 2 && !$from_club)))
            {
            	// ако личната отстъпка за всички продукти е >= на наличния промо баланс
            	// => използва се личния промо баланс
            	// else => използва се личната отстъпка за всички продукти
                $price_balance_promo = $user['balance_promo'];
            }
            else if ($user['balance_promo_valid_for'] === null
                || ($user['balance_promo_valid_for'] == 1 && $from_club)
                || ($user['balance_promo_valid_for'] == 2 && !$from_club))
            {
                $price_balance_promo = $price_balance_promo;
            }
            else
            {
                $price_balance_promo = 0;
            }
            

            $collection = Collection::get($order['collection_id']);
            if ($collection['fixed_price_delivery'] != 0)
            {
                $price_delivery = $collection['fixed_price_delivery'] * $products_quantity;
            }
            else
            {
                // ако платежния метод е наложен платеж
                /*if ($order['payment_method_id'] == 3)
                {
                    if ($price_products < 150)
                    {
                        $price_delivery = 5;
                    }
                }
                else 
                {
                	if ($price_products < 45)
                	{
                		$price_delivery = 5;
                	}
                }*/
                $price_delivery = Order::priceDelivery($order['payment_method_id'], $price_products, $price_balance_promo, $order['price_balance'], $promo_discount);
            }
        }
        else
        {
            $price_balance_promo = $order['price_balance_promo'];
            $price_delivery = $order['price_delivery'];
            $price_balance = $order['price_balance'];
        }
        

//         echo '$price_balance_promo = ' . $price_balance_promo . ' / $price_delivery = ' . $price_delivery . ' ///// ';
        // крайната сума
        $price_total = $price_products - $price_balance_promo + $price_delivery - $price_balance + $order['price_discount'];

        // променяне на стойностите
        $order['price_products'] = $price_products;
        $order['price_balance_promo'] = $price_balance_promo;
        $order['price_balance'] = $price_balance;
        $order['price_delivery'] = $price_delivery;
        $order['price_total'] = $price_total;

        if ($user['user_id'] > 0)
        {
            $sql = '
                UPDATE `orders`
                SET
                    `price_products` = ' . escape($order['price_products']) . '
                    ,`price_balance_promo` = ' . escape($order['price_balance_promo']) . '
                    ,`price_balance` = ' . escape($order['price_balance']) . '
                    ,`price_delivery` = ' . escape($order['price_delivery']) . '
                    ,`price_total` = ' . escape($order['price_total']) . '
                WHERE
                    `order_id` = ' . escape($order_id);
            $result = mysql_query($sql);
        }

        return $order;
    }

    /**
     * Връща `order_id`
     *
     * @param int $acronym
     * @return int
     */
    public static function getByAcronym($acronym, $user_id = 0)
    {
        $sql = 'SELECT `order_id` FROM `orders` WHERE `acronym` = ' . escape($acronym);
        $result = mysql_query($sql);

        $order_id = mysql_fetch_assoc($result);

        return Order::get($order_id['order_id'], $user_id);
    }

    /**
     * Връща активната поръчка
     *
     * @param int $user_id
     * @return int $order_id ако има активна поръчка
     * @return int 0 ако няма активна поръчка
     * @return false ако има грешка
     */
    public static function getActiveByUserId($user_id)
    {
        $sql = '
            SELECT `order_id`
            FROM `orders`
            WHERE
                `user_status` = 1
                AND `user_id` = ' . escape($_SESSION['user']['user_id']) . '
                AND `date_expire` > NOW()';
        $result = mysql_query($sql);

        if (mysql_num_rows($result) == 0)
        {
            // няма активна поръчка
            return 0;
        }
        elseif (mysql_num_rows($result) == 1)
        {
            // има активна поръчка
            $order = mysql_fetch_assoc($result);
            return $order['order_id'];
        }
        else
        {
            // грешка
            Email::sendError(__FILE__ . ' => ' . __LINE__);
            return FALSE;
        }
    }

    public static function create($user_id = 0, $collection_id = 0, $market_partner_id = 0)
    {
        $error = 0;

        if (!$user_id > 0)
        {
            $error = 1;
        }

        if ($error == 0)
        {
            $acronym = Order::insertAcronym();
            $user = User::get($user_id);

            $sql = '
            UPDATE
                `orders`
            SET
                `market_partner_id` = ' . escape($market_partner_id) . '
                ,`collection_id` = ' . escape($collection_id) . '
                ,`user_id` = ' . escape($user['user_id']) . '
                ,`reg_referred_by` = ' . escape($user['reg_referred_by']) . '
                ,`date_created` = NOW()
                ,`date_expire` = NOW() + INTERVAL ' . self::$cart_lifetime . ' MINUTE
                ,`referrer` = ' . escape($_SESSION['referrer']) . '
                ,`utm_source` = ' . escape($_SESSION['utm']['utm_source']) . '
                ,`utm_campaign` = ' . escape($_SESSION['utm']['utm_campaign']) . '
                ,`utm_medium` = ' . escape($_SESSION['utm']['utm_medium']) . '
            WHERE
                `acronym` = ' . escape($acronym);
            $result = mysql_query($sql) or $error = 1;
        }
        else
        {
            $error = 1;
        }

        if ($error == 1)
        {
            return FALSE;
        }
        else
        {
            return $acronym;
        }
    }

    public static function generateAcronym()
    {
        $characters = '0123456789';
        $string = '';
        for ($i = 1; $i <= 4; $i++)
        {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    public static function insertAcronym()
    {
        $error = 0;

        $acronym = '';
//         $acronym .= date('ymd');
//         $acronym .= Order::generateAcronym();

        $i = 0;
        
        do 
        {
        	$acronym = '';
        	$acronym .= date('ymd');
            $acronym .= Order::generateAcronym();
        	
	        $sql_check_acronym = 'SELECT COUNT(*) AS n FROM orders WHERE acronym = ' . escape($acronym);
	        $result_check_acronym = mysql_query($sql_check_acronym);
	        $data_check_acronym = mysql_fetch_assoc($result_check_acronym);
	        
	        if ($i == 5)
	        {
	        	mail('d.argirov@outlook.com', 'insertAcronym ERROR', 'Order::insertAcronym counter > 5');
	        	break;
	        }
	        
	        $i++;
        	
        } while($data_check_acronym['n'] > 0);
        
        

        $sql = 'INSERT INTO `orders` SET `acronym` = ' . escape($acronym);
        $result = mysql_query($sql) or $error = 1;

        if ($error == 1)
        {
            $acronym = Order::insertAcronym();
        }
        else
        {
            return $acronym;
        }
    }

    public static function getUserStatuses()
    {
        $sql = 'SELECT * FROM `data_order_user_status` ORDER BY `order_user_status_id` ASC';
        $result = mysql_query($sql);

        return $result;
    }

    public static function getUserStatus($order_user_status_id, $write_label = 0)
    {
        $sql = 'SELECT * FROM `data_order_user_status` WHERE `order_user_status_id` = ' . escape($order_user_status_id);
        $result = mysql_query($sql);

        $order_status = mysql_fetch_assoc($result);

        if ($write_label == 1)
        {

            return '<span class="label ' . $order_status['label'] . '">' . $order_status['name_bg'] . '</span>';
        }
        else
        {
            return $order_status;
        }
    }

    public static function getPaymentStatuses()
    {
        $sql = 'SELECT * FROM `data_order_payment_status` ORDER BY `order_payment_status_id` ASC';
        $result = mysql_query($sql);

        return $result;
    }

    public static function getPaymentStatus($order_payment_status_id, $write_label = 0)
    {
        $sql = 'SELECT * FROM `data_order_payment_status` WHERE `order_payment_status_id` = ' . escape($order_payment_status_id);
        $result = mysql_query($sql);

        $order_status = mysql_fetch_assoc($result);

        if ($write_label == 1)
        {

            return '<span class="label ' . $order_status['label'] . '">' . $order_status['name_bg'] . '</span>';
        }
        else
        {
            return $order_status;
        }
    }

    public static function getPaymentMethod($payment_method_id, $write_label = 0)
    {
        $sql = 'SELECT * FROM `data_payment_methods` WHERE `payment_method_id` = ' . escape($payment_method_id);
        $result = mysql_query($sql);

        $payment_method = mysql_fetch_assoc($result);

        if ($write_label == 1)
        {

            return '<span class="label ' . $payment_method['label'] . '">' . $payment_method['name_bg'] . '</span>';
        }
        else
        {
            return $payment_method;
        }
    }

    public static function getOrderStatuses()
    {
        $sql = 'SELECT * FROM `data_order_status` ORDER BY `order_status_id` ASC';
        $result = mysql_query($sql);

        return $result;
    }

    public static function getOrderStatus($order_status_id, $write_label = 0)
    {
        $sql = 'SELECT * FROM `data_order_status` WHERE `order_status_id` = ' . escape($order_status_id);
        $result = mysql_query($sql);

        $order_status = mysql_fetch_assoc($result);

        if ($write_label == 1)
        {

            return '<span class="label ' . $order_status['label'] . '">' . $order_status['name_bg'] . '</span>';
        }
        else
        {
            return $order_status;
        }
    }

    public static function getOrderConfirmationStatuses()
    {
        $sql = 'SELECT * FROM `data_order_confirmation_status` ORDER BY `order_confirmation_status_id` ASC';
        $result = mysql_query($sql);

        return $result;
    }

    public static function getOrderConfirmationStatus($order_confirmation_status_id, $write_label = 0)
    {
        $sql = 'SELECT * FROM `data_order_confirmation_status` WHERE `order_confirmation_status_id` = ' . escape($order_confirmation_status_id);
        $result = mysql_query($sql);

        $order_status = mysql_fetch_assoc($result);

        if ($write_label == 1)
        {

            return '<span class="label ' . $order_status['label'] . '">' . $order_status['name_bg'] . '</span>';
        }
        else
        {
            return $order_status;
        }
    }

    public static function cronUpdateExpiredOrders()
    {
        $error = 0;

//         UPDATE 15.05.2014
//         $sql = '
//             SELECT `order_id`
//             FROM `orders`
//             WHERE
//                 `user_status` IN ( 1, 2, 3 )
//                 AND `date_expire` <= NOW() + INTERVAL 5 SECOND';
        $sql = '
            SELECT `order_id`
            FROM `orders`
            WHERE
                `user_status` IN ( 1, 3 )
                AND `date_expire` <= NOW() + INTERVAL 5 SECOND';
        $result = mysql_query($sql) or $error = 1;

        if (mysql_num_rows($result) > 0)
        {
            while ($order = mysql_fetch_assoc($result))
            {
                $sql_update_orders = 'UPDATE `orders` SET `user_status` = 2 WHERE `order_id` = ' . escape($order['order_id']);
                $result_update_orders = mysql_query($sql_update_orders) or $error = 2;

                $sql_update_order_products = 'UPDATE `order_products` SET `reserved` = 0 WHERE `order_id` = ' . escape($order['order_id']);
                $result_update_order_products = mysql_query($sql_update_order_products) or $error = 3;
//                 mail('d.argirov@outlook.com', 'cronUpdateExpiredOrders', 'Order ID: ' . $order['order_id'] . ' Time: ' . date('d.m.Y: H;i'));
            }
        }

        if ($error > 0)
        {
            Email::sendError(__FILE__ . ' => Error:' . $error);
        }
    }
    
    public static function cronUpdateExpiredRequestedOrders()
    {
    	$error = 0;
    
    	$sql = '
            SELECT `order_id`
            FROM `orders`
            WHERE
                `user_status` = 4
    			AND `payment_status` = 1
    			AND `confirmation_status` = 1
    			AND `payment_method_id` IN ( 1, 4, 5, 6, 8 ) 
                AND `payment_easypay_date_expire` <= NOW()';
    	$result = mysql_query($sql) or $error = 1;
    
    	if (mysql_num_rows($result) > 0)
    	{
    		while ($order = mysql_fetch_assoc($result))
    		{
    			$sql_update_orders = 'UPDATE `orders` SET `payment_status` = 5 WHERE `order_id` = ' . escape($order['order_id']);
    			$result_update_orders = mysql_query($sql_update_orders) or $error = 2;
    
    			$sql_update_order_products = 'UPDATE `order_products` SET `reserved` = 0 WHERE `order_id` = ' . escape($order['order_id']);
    			$result_update_order_products = mysql_query($sql_update_order_products) or $error = 3;
    			
    			#mail('office@dmediabulgaria.com', 'cronUpdateExpiredRequestedOrders', 'Order ID: ' . $order['order_id']);
    			mail('d.argirov@outlook.com', 'cronUpdateExpiredRequestedOrders', 'Order ID: ' . $order['order_id'] . ' Time: ' . date('d.m.Y: H;i'));
    		}
    	}
    
    	if ($error > 0)
    	{
    		Email::sendError(__FILE__ . ' => Error:' . $error);
    	}
    }

    /**
     * Връща цени и отстъпки.
     * Ако се изпрати `user_id`, ще сметне персонална отстъпка!
     *
     * [price_discount] => 85 / 85% отстъпка
     * [price_personal] => 50,90 / персонална цена за този product/size_variant_id, след приспадане на max_promo_balance
     * [price_personal_discount] => 5,00 / колко е извадена от цената от промо баланса
     * [price_listing] => 50,90 / цена която да се ползва като най-ниска при листинг на продукта
     * [price_customer] => 55,90 / цената на която продаваме
     * [price_supplier_mrsp] => 114,00
     * [max_promo_balance] => 5,00
     *
     * @param int $product_id
     * @param int $size_variant_id
     * @param int $user_id
     * @param int $formatted default 1
     *
     * @return array `price_discount` - процент отстъпка /персонализиран или не/, само число
     * @return array `price_personal` - ако е 0, значи няма персонална отстъпка
     * @return array `price_personal_discount` - ако е 0, значи няма персонална отстъпка, левова равностойност на сумата използвана от промо Ваучера
     */
    public static function getProductVariantPrices($product_id, $size_variant_id, $user_id = 0, $formatted = 1)
    {
        $sql_product = '
            SELECT *
            FROM `product_variants`
            WHERE
                `product_id` = ' . escape($product_id) . '
                AND `size_variant_id` = ' . escape($size_variant_id);
        $result_product = mysql_query($sql_product);
        $product = mysql_fetch_assoc($result_product);

        // calculate the discount percentage
        if ($product['price_supplier_mrsp'] > $price['price_customer'])
        {
            $price['price_discount'] = 100 - (($product['price_customer'] / $product['price_supplier_mrsp']) * 100);
        }
        else
        {
            $price['price_discount'] = 0;
        }


        $price['price_personal'] = 0;
        $price['price_personal_discount'] = 0;

        if ($user_id > 0)
        {
            $sql_user = 'SELECT * FROM `users` WHERE `user_id` = ' . escape($user_id);
            $result_user = mysql_query($sql_user);
            $user = mysql_fetch_assoc($result_user);

            if ($user['balance_promo'] > 0)
            {
                if ($user['balance_promo'] > $product['max_promo_balance'])
                {
                    $price['price_personal'] = $product['price_customer'] - $product['max_promo_balance'];
                }
                else
                {
                    $price['price_personal'] = $product['price_customer'] - $user['balance_promo'];
                }

                $price['price_personal_discount'] = $product['price_customer'] - $price['price_personal'];
                $price['price_listing'] = $price['price_personal'];

                // calculate personal the discount percentage
                $price['price_discount'] = 100 - (($price['price_personal'] / $product['price_supplier_mrsp']) * 100);
            }
            else
            {
                $price['price_listing'] = $product['price_customer'];
            }
        }
        else
        {
            $price['price_listing'] = $product['price_customer'];
        }

        $price['price_customer'] = $product['price_customer'];
        $price['price_supplier_mrsp'] = $product['price_supplier_mrsp'];
        $price['max_promo_balance'] = $product['max_promo_balance'];

        if ($formatted == 1)
        {
            foreach ($price as $key => $value)
            {
                $price[$key] = number_format($value, 2, ',', ' ');
                $price[$key . '_decimal'] = $value;
            }

            $price['price_discount'] = number_format($price['price_discount'], 0);

            return $price;
        }
        else
        {
            return $price;
        }
    }

    public static function returnGeneratedSaleOrder($acronym)
    {
        $sql = 'SELECT * FROM `sale_order_orders` WHERE `order_id` = ' . escape($acronym);
        $result = mysql_query($sql);

        if (mysql_num_rows($result) == 0)
        {
            return 0;
        }
        elseif (mysql_num_rows($result) == 1)
        {
            $sale_order = mysql_fetch_assoc($result);
            return $sale_order['sale_order_id'];
        }
        else
        {
            return FALSE;
        }
    }

    public static function returnGeneratedSaleOrderJan($acronym)
    {
        $sql = 'SELECT * FROM `sale_order_orders_jan` WHERE `order_id` = ' . escape($acronym);
        $result = mysql_query($sql);

        if (mysql_num_rows($result) == 0)
        {
            return 0;
        }
        elseif (mysql_num_rows($result) == 1)
        {
            $sale_order = mysql_fetch_assoc($result);
            return $sale_order['sale_order_id'];
        }
        else
        {
            return FALSE;
        }
    }
    
    
    public static function getOrderQuantity($order_id, $product_id, $variant_internal)
    {
    	$sql = '
            SELECT `quantity`
            FROM `order_products`
            WHERE
                `product_id` = ' . escape($product_id) . '
                AND `order_id` = ' . escape($order_id) . '
                AND `variant_internal` = ' . escape($variant_internal);
    	$result = mysql_query($sql);
    	 
    	if (mysql_num_rows($result) == 1)
    	{
    		$data = mysql_fetch_assoc($result);
    		return $data['quantity'];
    	}
    	return FALSE;
    }
    
    
    public static function getOrderAcronym($order_id)
    {
    	$sql = '
            SELECT `acronym`
            FROM `orders`
            WHERE
                `order_id` = ' . escape($order_id);
    	$result = mysql_query($sql);
    
    	if (mysql_num_rows($result) == 1)
    	{
    		$data = mysql_fetch_assoc($result);
    		return $data['acronym'];
    	}
    	return FALSE;
    }
    
    
    public static function removeItemFromCart($order_product_id)
    {
    	/*$sql = 'SELECT `order_id` FROM `orders` WHERE `user_id` = ' . escape($_SESSION['user']['user_id']) . ' AND `acronym` = ' . escape($acronym);
    	$result = mysql_query($sql);
    	if (mysql_num_rows($result) == 1)
    	{
    		$data = mysql_fetch_assoc($result);
    		$order_id = $data['order_id'];
    	}
    	else
    	{
    		return FALSE;
    	}*/
    	
    	if (count($_SESSION['order']['products']) > 1)
    	{
    		$sql = 'UPDATE `order_products` SET `reserved` = 0 WHERE `order_product_id` = ' . escape($order_product_id);;
    		$result = mysql_query($sql) or die(mysql_error());
    		 
    	}
    	else
    	{
    		$sql = 'UPDATE `orders` SET `user_status` = 3 WHERE `order_id` = ' . escape($_SESSION['order']['order_id']);
    		$result = mysql_query($sql) or die(mysql_error());
    		
    		$sql = 'UPDATE `order_products` SET `reserved` = 0 WHERE `order_id` = ' . escape($_SESSION['order']['order_id']);;
    		$result = mysql_query($sql) or die(mysql_error());
    		 
    	}
    	
    	
//     	unset($_SESSION['order']);
//     	$_SESSION['order'] = Order::get($order_id, $_SESSION['user']['user_id']);
    	return TRUE;
    }

}